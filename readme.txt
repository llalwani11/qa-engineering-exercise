PART 3 DOCUMENTATION

The test case written is in part 3 is only for verifying whether an issue can be created. It does not verify that different issue settings have no problems. It is intended to check whether an issue can be created with the minimum possible settings changed from the default (only specifying the issue title). It is assumed that the user is using the ‘Create’ button on the menu to create the issue from the main project page (“https://jira.atlassian.com/browse/TST”).

A prerequisite here is that the user is logged in and has permission to create issues in the test project (A Test Project).