import static org.junit.Assert.*;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

/** This is a class to test the Project Home page
 *
 * @author Lavish Lalwani
 */
public class ProjectPage {
	
    private final WebDriver driver;

    public ProjectPage(WebDriver driver) {
        this.driver = driver;
        // Check that we're on the right page.
        if (!"A Test Project - Atlassian JIRA".equals(driver.getTitle())) {
            throw new IllegalStateException("This is not the project page");
        }
    }

    // These are element locators required for the tests
    By createIssueSubmitLocator = By.id("create-issue-submit");
    By summaryLocator = By.id("summary");
    By createButtonLocator = By.id("create_link");
    By messageBox = By.className("issue-created-key");

    // Action to click on the create link button
    public ProjectPage clickCreateIssue() {
        driver.findElement(loginButtonLocator).click();
        return this;    
    }

    // Action to type a summary in the create issue popup
    public ProjectPage typeIssueSummary(String summary) {
        driver.findElement(summaryLocator).sendKeys(summary);
        return this;    
    }

    // Action to submit the request to create an issue
    public ProjectPage submitIssueCreation() {
        driver.findElement(createIssueSubmitLocator).submit();
        return new HomePage(driver);    
    }

    // This conceptually runs through the issue creation process
    public ProjectPage createBasicIssue(String summary) {
        clickCreateIssue();
        typeIssueSummary(summary);
        return submitIssueCreation();
    }

    // Get the message after an action is performed on the page
    public String getPageMessage() {
        String message = driver.findElement(messageBox).getAttribute("innerHTML");
        return message;
    }
}

/** This is a method to test the issue creation process */
public void testIssueCreation() {
    String summary = "This is the summary";
    ProjectPage project = new ProjectPage(driver);
    project.createBasicIssue(summary);
    try {
        String message = (project.getPageMessage().split('-'))[1];
    } catch (Exception e) {
        assert false;
    }
    assert (message.equals(" " + summary));
}